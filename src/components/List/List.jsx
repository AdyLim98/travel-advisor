import React,{ useState,useEffect,createRef } from 'react';
import { CircularProgress , Grid ,Typography , InputLabel ,MenuItem ,FormControl ,Select} from '@material-ui/core';
import PlaceDetails from '../PlaceDetails/PlaceDetails';

import useStyle from './style';

// To use ref , u need
// 1) createRef first

//ScrollIntoView feature havennt done yet
const List = ({places,childClicked,isLoading,type,setType,rating,setRating}) =>{
    const classes = useStyle()

    const [elRefs, setElRefs] = useState([]);

    // const [type,setType] = useState("restaurants");
    // const [rating,setRating] = useState("");
   
    //use the ref is to when click the card on map ,then it will scroll to the card list which u click just now
    // const [elementRef,setElementRef] = useState([])

    console.log({childClicked})
    console.log({type})

    var Places = []
    if(places.length){
        for(var x of places){
            if(x.name) Places.push(x)
    }}
    console.log("Places:",Places)

    //refresh when places is changed
    // useEffect(()=>{
        // Array below means want array format ,let say places array length 30 : [30]
        // Array.fill() use to fill in the data into the array
        // _ means ignore the first 1
        // if no exist elementRef[i] then create ref
       
        // const refs = Array(Places.length).fill().map((_,i)=>elementRef[i] || createRef())
        // setElementRef(refs)
        // OR
        // setElementRef((refs) => Array(Places.length).fill().map((_, i) => refs[i] || createRef()));

        //after store refs inside the usestate then can use the ref now :) inside the interface
    // },[Places])
    // console.log("elref:",elementRef)
    
    useEffect(() => {
        setElRefs((refs) => Array(places.length).fill().map((_, i) => refs[i] || createRef()));
    }, [places]);

    return(
        <div className={classes.container}>
            <Typography variant="h5">
                Restaurants , Hotels & Attractions Around You
            </Typography>

            {isLoading?(
                <div className={classes.loading}>
                    <CircularProgress size="5rem"/>
                </div>
                ):(
            <>
                <div>
                    <p style={{fontSize:'18px',fontWeight:600}}>Type
                    <span style={{paddingLeft:'45px'}}>
                        <FormControl className={classes.formControl} style={{marginTop:"-3px",backgroundColor:"mediumslateblue"}}fullwidth>
                            <Select value={type} onChange={(e)=>setType(e.target.value)} style={{color:'white',paddingLeft:'10px'}}>
                                <MenuItem value="restaurants">Restaurants</MenuItem>
                                <MenuItem value="hotels">Hotels</MenuItem>
                                <MenuItem value="attractions">Attractions</MenuItem>
                            </Select>
                        </FormControl>
                    </span>
                    </p>
                </div>
                <div>
                    <p style={{fontSize:'18px',fontWeight:600}}>Rating
                    <span style={{paddingLeft:'32px'}}>
                        <FormControl className={classes.formControl} style={{marginTop:"-3px",backgroundColor:"mediumslateblue"}}fullwidth>
                            <Select value={rating} onChange={(e)=>setRating(e.target.value)} style={{color:'white',paddingLeft:'10px'}}>
                                <MenuItem value={0}>All</MenuItem>
                                <MenuItem value={3}>Above 3 Stars</MenuItem>
                                <MenuItem value={4}>Above 4 Stars</MenuItem>
                                <MenuItem value={5}>Above 4.5 Stars</MenuItem>
                            </Select>
                        </FormControl>
                    </span>
                    </p>
                </div>

                {(!(places.length)) && (<div className={classes.loading}>
                    <CircularProgress size="5rem"/>
                </div>)}
                
                <Grid container spacing={3} className={classes.list}>
                    {/* ?. means if got places then just can map (if statement) */}
                    {(places.length) && Places?.map((place,i)=>(
                        //can write as below -> import the ref component or
                        // <Grid item xs={12} key={i} ref={elementRef[i]}>
                        <Grid item xs={12} key={i} ref={elRefs[i]}>
                            <PlaceDetails place={place}
                                //selected when childClicked (array Id) is same as i here
                                selected={Number(childClicked) === i} refProp={elRefs[i]}
                                // refProps={elementRef[i]}
                            />
                        </Grid>  
                    ))}
                </Grid>
            </>
            )}
        </div>
    )
}

export default List;