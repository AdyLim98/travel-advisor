import React from 'react';

import { Box , Typography ,Button , Card , CardMedia , CardContent ,CardActions ,Chip} from '@material-ui/core';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import PhoneIcon from '@material-ui/icons/Phone';
import Rating from '@material-ui/lab/Rating';

import useStyle from './style';
// ,refProps
const PlaceDetails = ({place,selected,refProp}) =>{
    if (selected) refProp?.current?.scrollIntoView({ behavior: 'smooth', block: 'start' });
    
    const classes = useStyle();
    console.log("Selected:",selected)
    // console.log("SelectedRefProps:",refProps)
    
    // scrollIntoView is the basic javascript function like javascript method like split,map,reduce,etc...
    // if(selected) refProps?.current?.scrollIntoView({behavior:"smooth",block:"start"})

    return(
        <Card>
            <CardMedia image={place.photo ? place.photo.images.large.url : "https://upload.wikimedia.org/wikipedia/commons/e/ef/Restaurant_N%C3%A4sinneula.jpg" } style={{height:350}} />
            <CardContent>
                <Typography gutterBottom variant="h5">{place.name}</Typography>
                <Box display="flex" justifyContent="space-between">
                    <Rating  value={Number(place.rating)} readOnly/>
                    <Typography variant="subtitle1">Out of {place.num_reviews} Reviews</Typography>
                </Box>
                <Box display="flex" justifyContent="space-between">
                    <Typography variant="subtitle1">Price</Typography>
                    <Typography variant="subtitle1">{place.price_level}</Typography>
                </Box>
                <Box display="flex" justifyContent="space-between">
                    <Typography variant="subtitle1">Ranking</Typography>
                    <Typography variant="subtitle1" gutterBottom>{place.ranking}</Typography>
                </Box>
                {/* if got place.awards */}
                {place?.awards?.map((award)=>(
                    <Box display="flex" justifyContent="space-between" style={{marginTop:5}}>
                        <img src={award.images.small}/>  
                        <Typography variant="subtitle1">{award.display_name}</Typography>  
                    </Box>
                ))}
                {place?.cuisine?.map(({name})=>(
                    <Chip key={name} size="small" label={name} className={classes.chip} style={{marginTop:10}}/>
                ))}
                {/* code below also can write as place?.address */}
                {place.address && (
                    <Typography gutterBottom variant="subtitle2" color="textSecondary" className={classes.subtitle}>
                        <LocationOnIcon/><span style={{fontSize:10}}>{place.address}</span>
                    </Typography>
                )}
                {place?.phone && (
                    <Typography gutterBottom variant="subtitle2" color="textSecondary" className={classes.spacing}>
                        <PhoneIcon style={{fontSize:20}}/>{place.phone}
                    </Typography>
                )}
                <CardActions style={{display:"flex" ,justifyContent:"space-between"}}>
                    {/* window.open function will navigate to the url page u set , '_blank' options means will create a new page with the web_url 
                    which means when u click the Reviews button , then it will not just go to the website with same page ,otherwise will go to reviews page with the new page */}
                    <Button size="small" color="primary" variant="outlined" onClick={()=>window.open(place.web_url,'_blank')}>
                        Go To Reviews
                    </Button>
                    <Button size="small" color="primary" variant="outlined" onClick={()=>window.open(place.website,'_blank')}>
                        Website
                    </Button>
                </CardActions>
            </CardContent>
        </Card>
    )
}

export default PlaceDetails;