import axios from 'axios';

const URL = ''
  
export const getPlaces = async (sw,ne,type) =>{
    try{
      console.log("HERE:",sw,ne)
      const { data:{data}} = await axios.get(`https://travel-advisor.p.rapidapi.com/${type}/list-in-boundary`,{
        params: {
          bl_latitude: sw.lat,
          tr_latitude: ne.lat,
          bl_longitude: sw.lng,
          tr_longitude: ne.lng,
        },
        headers: {
          'x-rapidapi-host': 'travel-advisor.p.rapidapi.com',
          'x-rapidapi-key': '6d2c2bf990msh1f823a13d72cf9cp174497jsnf267fa5d8178',
          //code below is used to solve the cors policy due to company pc cannot call the api
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
        }
      });
      return data ;
    }catch(error){
        console.log(error)
        return error;
    }
}

export const getWeather = async (lat,lng) =>{
  try{
    const {data} = axios.get('https://community-open-weather-map.p.rapidapi.com/find',{
    params: {
      lat:"2.039272" , lon: "102.569092" 
    },
    headers: {
      'x-rapidapi-host': 'community-open-weather-map.p.rapidapi.com',
      'x-rapidapi-key': '6d2c2bf990msh1f823a13d72cf9cp174497jsnf267fa5d8178'
    }})
    return data
  }catch(err){
    console.log(err)
  }
}