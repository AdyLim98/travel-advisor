import React,{useState} from 'react';
import GoogleMapReact from 'google-map-react';
//useMediaQuery is used to make it more mobile responsive
import {Paper ,Typography,useMediaQuery} from '@material-ui/core';
import LocationOutlinedIcon from '@material-ui/icons/LocationOnOutlined';
import Rating from '@material-ui/lab/Rating';

import useStyles from './style';

const Map = ({coordinate,setCoordinate,setBounds,places,setChildClicked,weatherData }) =>{
    const classes = useStyles();
    //isMobile is false when device is larger than width 600px
    const isDesktop = useMediaQuery('(min-width:600px)');
    const coordinates = {
        lat:2.039272,
        lng:102.569092
    }
    // Code below move up a level so that all the screens can use the data
    // const [childClicked,setChildClicked] = useState(null)
    //console log below will get output in object : {childClicked:5} let says u click the card on Map is array id #5
    // console.log({childClicked})
    // console.log({weatherData})
    return(
        <div className={classes.mapContainer}>
            <GoogleMapReact
                bootstrapURLKeys={{key:'AIzaSyDJ3dikD73cE33SD9KsuLqDSwejmBmy5pM'}}
                // defaultCenter={coordinates}
                // center={coordinates}
                defaultCenter={coordinate}
                center={coordinate}
                defaultZoom={14}
                // margin={[50,50,50,50]}
                // options={''}
                onChange={(e)=>{
                    // console.log(e)
                    setCoordinate({lat:e.center.lat,long:e.center.lng})
                    setBounds({ne:e.marginBounds.ne,sw:e.marginBounds.sw})
                }}
                //onChildClick use to click the restaurant on map
                onChildClick={(child)=>setChildClicked(child)}
            >
                {/* {console.log("PlacesCard:",places)} */}
                {(places.length) && places?.map((place,i)=>(
                    <div
                        className={classes.markerContainer}
                        // I think div inside can lat/lng is due to the google map react package
                        // convert to Number
                        lat={Number(place.latitude)}
                        lng={Number(place.longitude)}
                        key={i}
                    >
                        {!isDesktop ? (<LocationOutlinedIcon color="primary" fontSize="large"/>)
                            : (
                            <Paper elevation={3} className={classes.paper}>
                                <Typography className={classes.typography} variant="subtitle2" gutterBottom>
                                    {place.name}
                                </Typography>
                                <img className={classes.pointer}
                                    src={place.photo ? place.photo.images.large.url : "https://upload.wikimedia.org/wikipedia/commons/e/ef/Restaurant_N%C3%A4sinneula.jpg"}
                                />
                                <Rating size="small" value={Number(place.rating)} readOnly/>
                            </Paper>
                            )
                        }

                    </div>
                ))}
                {/* {weatherData?.list?.length && weatherData.list.map((data, i) => (
                    <div key={i} lat={data.coord.lat} lng={data.coord.lon}>
                        <img src={`http://openweathermap.org/img/w/${data.weather[0].icon}.png`} height="70px" />
                    </div>
                ))} */}
            </GoogleMapReact>
        </div>
    )
}

export default Map;
