import React,{useState,useEffect} from 'react';
import { CssBaseline , Grid } from '@material-ui/core';

import { Header , List , Map } from './components/index';
import { getPlaces,getWeather } from './api/index';

const App = () =>{
    const [places,setPlaces] = useState([]);
    const [coordinate,setCoordinate] = useState({});
    const [bounds,setBounds] = useState(null);
    const [childClicked,setChildClicked] = useState(null)
    const [isLoading,setIsLoading] = useState(false)
    const [type,setType] = useState("restaurants");
    
    const [rating,setRating] = useState("");
    const [filterPlaces,setFilteredPlaces] = useState([]);

    const [weatherData, setWeatherData] = useState([]);

    useEffect(()=>{
        //navigator.geolocation.getCurrentPosition is the function provided
        //by browser automatically ,u no nid import any library to trigger this function
        //Browser will request the user the current location(a pop request message when open it) by click "ok" or "cancel"
        navigator.geolocation.getCurrentPosition(({coords:{latitude,longitude}})=>{
            // console.log(e.coords)
            setCoordinate({lat:latitude,lng:longitude})
        })
    },[])

    useEffect(()=>{
        const filteredPlaces = places.filter((place)=>place.rating > rating)

        setFilteredPlaces(filteredPlaces)
    },[rating])

    // lat:2.039272
    // lng:102.569092
    useEffect(()=>{ 
        if(!bounds){
            var sw = {lat:2.0638157061654283,lng:102.58708632878529}
            var ne = {lat:2.0046298560060336,lng:102.5559297888683}
            setIsLoading(true)
            
            getWeather(coordinate.lat, coordinate.lng)
            .then((data) => setWeatherData(data));

            getPlaces(sw,ne,type).then((response)=>{
                setPlaces(response)
                setFilteredPlaces([])
                setIsLoading(false)
            })  
        }else{
            setIsLoading(true)

            getWeather(coordinate.lat, coordinate.lng)
            .then((data) => setWeatherData(data));

            getPlaces(bounds.sw,bounds.ne,type).then((response)=>{
                setPlaces(response)
                setFilteredPlaces([])
                setIsLoading(false)
            })
        }
    },[coordinate,bounds,type])
    console.log("Places:",places)
    return(
        <div>
            <CssBaseline />
            <Header setCoordinate={setCoordinate}/>
            <Grid container spacing={3} style={{width:'100%'}}>
                <Grid item xs={12} md={4}>
                    <List places={filterPlaces.length ? filterPlaces : places}
                        childClicked={childClicked} 
                        isLoading={isLoading}
                        type={type}
                        setType={setType}
                        rating={rating}
                        setRating={setRating}
                    />
                </Grid>
                <Grid item xs={12} md={8} style={{width:'100%',padding:'25px 0px 12px 12px'}}>
                    <Map setCoordinate={setCoordinate} setBounds={setBounds}
                        coordinate={coordinate}
                        places={filterPlaces.length ? filterPlaces : places}
                        setChildClicked={setChildClicked}
                        weatherData={weatherData}/>
                </Grid>
            </Grid>
        </div>
    )
}

export default App;